package it.unina.ingsw.ristorantegiapponese;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import it.unina.ingsw.ristorantegiapponese.database.DBManager;

public class InizializzaDB {

	public static void main(String[] args) {

		Connection conn = DBManager.getConnection();
		
		ArrayList<String> sqlqueries = new ArrayList<String>();
			
		sqlqueries.add("CREATE TABLE Piatti( \n" + 
				"  ID INT NOT NULL AUTO_INCREMENT,\n" + 
				"  Tipo VARCHAR(30) NOT NULL,\n" + 
				"  Prezzo INT NOT NULL,\n" + 
				"  Ingrediente VARCHAR(30) NOT NULL,\n" + 
				"  NumeroPezzi INT NOT NULL,\n" + 
				"  PRIMARY KEY(\"ID\")\n" + 
				");");
		
		sqlqueries.add("CREATE TABLE Bevande( \n" + 
				"  ID INT NOT NULL AUTO_INCREMENT,\n" + 
				"  Tipo VARCHAR(30) NOT NULL,\n" + 
				"  Prezzo INT NOT NULL,\n" + 
				"  PRIMARY KEY(\"ID\")\n" + 
				");");
		
		sqlqueries.add("INSERT INTO Piatti VALUES (NULL, 'SUSHI', 5, 'TONNO', 2);");
		sqlqueries.add("INSERT INTO Piatti VALUES (NULL, 'SUSHI', 5, 'SALMONE', 2);");
		sqlqueries.add("INSERT INTO Piatti VALUES (NULL, 'SUSHI', 5, 'ANGUILLA', 2);");
		sqlqueries.add("INSERT INTO Piatti VALUES (NULL, 'SUSHI', 5, 'GRANCHIO', 2);");
		sqlqueries.add("INSERT INTO Piatti VALUES (NULL, 'SASHIMI', 6, 'TONNO', 4);");
		sqlqueries.add("INSERT INTO Piatti VALUES (NULL, 'SASHIMI', 6, 'SALMONE', 4);");
		sqlqueries.add("INSERT INTO Bevande VALUES (NULL, 'ACQUA_LISCIA', 2);");
		sqlqueries.add("INSERT INTO Bevande VALUES (NULL, 'ACQUA_GASSATA', 2);");
		sqlqueries.add("INSERT INTO Bevande VALUES (NULL, 'ARANCIATA', 1);");
		sqlqueries.add("INSERT INTO Bevande VALUES (NULL, 'COLA', 1);");
		sqlqueries.add("INSERT INTO Bevande VALUES (NULL, 'BIRRA_GIAPPONESE', 5);");
		
		try {
			for(String query : sqlqueries) {
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.executeUpdate();
			}
		}
		catch(SQLException e) {

			e.printStackTrace();
		}	
		

		
	}

}
