package it.unina.ingsw.ristorantegiapponese.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import it.unina.ingsw.ristorantegiapponese.entity.*;
import it.unina.ingsw.ristorantegiapponese.control.*;
import it.unina.ingsw.ristorantegiapponese.boundary.*;


public class ristoranteGiapponeseTest {

	private GestoreOrdiniRistorante gestore_ord; 
	ArrayList<Piatto> piattiInMenu; 
	ArrayList<Bevanda> bevandeInMenu; 
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		gestore_ord = new GestoreOrdiniRistorante(); 
		piattiInMenu = new GestoreOrdiniRistorante().getPiattiInMenu(); 
		bevandeInMenu = new GestoreOrdiniRistorante().getBevandeInMenu(); 
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void test01formulaAllYouCanEatConBevandeUnCoperto() {
		
		int prezzoTotale; 
		
		Conto conto = gestore_ord.apriConto(new Tavolo(), new Cameriere(), Formula.ALL_YOU_CAN_EAT, 1); 
		
		Piatto[] piatti = new Piatto[1]; 
		
		piatti[0] = piattiInMenu.get(0); 
		
		int[] qtaPiatti = {1}; 
		
		Bevanda[] bevande = new Bevanda[1]; 
		
		bevande[0] = bevandeInMenu.get(0); 
		
		int[] qtaBevande = {1}; 
		
		gestore_ord.creaComanda(conto, piatti, qtaPiatti, bevande, qtaBevande);
		
		gestore_ord.chiudiConto(conto);
		
		prezzoTotale = gestore_ord.visualizzaConto(conto); 
		
		
	}
	
	@Test
	public void test02formulaAllYouCanEatConBevandeDueCoperto() {
		
		int prezzoTotale; 
		
		Conto conto = gestore_ord.apriConto(new Tavolo(), new Cameriere(), Formula.ALL_YOU_CAN_EAT, 2); 
		
		Piatto[] piatti = new Piatto[1]; 
		
		piatti[0] = piattiInMenu.get(0); 
		
		int[] qtaPiatti = {1}; 
		
		Bevanda[] bevande = new Bevanda[1]; 
		
		bevande[0] = bevandeInMenu.get(0); 
		
		int[] qtaBevande = {1}; 
		
		gestore_ord.creaComanda(conto, piatti, qtaPiatti, bevande, qtaBevande);
		
		gestore_ord.chiudiConto(conto);
		
		prezzoTotale = gestore_ord.visualizzaConto(conto); 
		
		
	}
	
	@Test
	public void test03formulaAllYouCanEatConBevandeUnCopertoPiuComande() {
		
		int prezzoTotale; 
		
		Conto conto = gestore_ord.apriConto(new Tavolo(), new Cameriere(), Formula.ALL_YOU_CAN_EAT, 1); 
		
		Piatto[] piatti = new Piatto[1]; 
		
		piatti[0] = piattiInMenu.get(0); 
		
		int[] qtaPiatti = {1}; 
		
		Bevanda[] bevande = new Bevanda[1]; 
		
		bevande[0] = bevandeInMenu.get(0); 
		
		int[] qtaBevande = {1}; 
		
		gestore_ord.creaComanda(conto, piatti, qtaPiatti, bevande, qtaBevande);
		
		gestore_ord.creaComanda(conto, piatti, qtaPiatti, bevande, qtaBevande);
		
		gestore_ord.chiudiConto(conto);
		
		prezzoTotale = gestore_ord.visualizzaConto(conto); 
		
		
	}
	
	
	@Test
	public void test04formulaAllYouCanEatConBevandeDueCopertoPiuComande() {
		
		int prezzoTotale; 
		
		Conto conto = gestore_ord.apriConto(new Tavolo(), new Cameriere(), Formula.ALL_YOU_CAN_EAT, 2); 
		
		Piatto[] piatti = new Piatto[1]; 
		
		piatti[0] = piattiInMenu.get(0); 
		
		int[] qtaPiatti = {1}; 
		
		Bevanda[] bevande = new Bevanda[1]; 
		
		bevande[0] = bevandeInMenu.get(0); 
		
		int[] qtaBevande = {1}; 
		
		gestore_ord.creaComanda(conto, piatti, qtaPiatti, bevande, qtaBevande);
		
		gestore_ord.creaComanda(conto, piatti, qtaPiatti, bevande, qtaBevande);
		
		gestore_ord.chiudiConto(conto);
		
		prezzoTotale = gestore_ord.visualizzaConto(conto); 
		
		
	}
	

	@Test
	public void test05formulaAllYouCanEatNoBevandeDueCopertoPiuComande() {
		
		int prezzoTotale; 
		
		Conto conto = gestore_ord.apriConto(new Tavolo(), new Cameriere(), Formula.ALL_YOU_CAN_EAT, 3); 
		
		Piatto[] piatti = new Piatto[1]; 
		
		piatti[0] = piattiInMenu.get(0); 
		
		int[] qtaPiatti = {1}; 
		
		Bevanda[] bevande = new Bevanda[1]; 
		
		bevande[0] = bevandeInMenu.get(0); 
		
		int[] qtaBevande = {0}; 
		
		gestore_ord.creaComanda(conto, piatti, qtaPiatti, bevande, qtaBevande);
		
		gestore_ord.creaComanda(conto, piatti, qtaPiatti, bevande, qtaBevande);
		
		gestore_ord.chiudiConto(conto);
		
		prezzoTotale = gestore_ord.visualizzaConto(conto); 
		
		
	}
	
	
	@Test
	public void test06formulaAllYouCanEatNessunCoperto() {
		
		int prezzoTotale; 
		
		Conto conto = gestore_ord.apriConto(new Tavolo(), new Cameriere(), Formula.ALL_YOU_CAN_EAT, 0); 
		
		gestore_ord.chiudiConto(conto);
		
		prezzoTotale = gestore_ord.visualizzaConto(conto); 
		
		
	}
	
	
	@Test
	public void test07formulaAllYouCanEatNessunaComanda() {
		
		int prezzoTotale; 
		
		Conto conto = gestore_ord.apriConto(new Tavolo(), new Cameriere(), Formula.ALL_YOU_CAN_EAT, 1); 
		
		gestore_ord.chiudiConto(conto);
		
		prezzoTotale = gestore_ord.visualizzaConto(conto); 
		
		
	}
	
	

}
