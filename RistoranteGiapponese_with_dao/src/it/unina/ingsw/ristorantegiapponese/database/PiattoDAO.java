package it.unina.ingsw.ristorantegiapponese.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;

import it.unina.ingsw.ristorantegiapponese.entity.Ingrediente;
import it.unina.ingsw.ristorantegiapponese.entity.Piatto;
import it.unina.ingsw.ristorantegiapponese.entity.TipoPiatto;

public class PiattoDAO {

	public static Piatto create(TipoPiatto tipo, Ingrediente ingrediente, int numeroPezzi, int prezzo) throws DAOException {
		
		Piatto piatto = new Piatto(tipo, ingrediente, numeroPezzi, prezzo);
		
		int id_piatto = -1;
		
		Connection conn = DBManager.getConnection();

		String sqlquery = "INSERT INTO Piatti VALUES (NULL, ?, ?, ?, ?);";
		
		try( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{
			
			stmt.setString(1, tipo.toString());
			stmt.setString(2, ingrediente.toString());
			stmt.setInt(3, numeroPezzi);
			stmt.setInt(4, prezzo);
			
			stmt.executeUpdate();

			try( ResultSet result = stmt.getGeneratedKeys(); )
			{
		        if (result.next()) {
		        	id_piatto = result.getInt(1);
		        }
			}
		}
		catch(SQLException e) {
			
			throw new DAOException("Errore INSERT Piatto");
		}
		
		piatto.setId(id_piatto);

		return piatto;
	}
	
	
	
	public static Piatto read(Integer id) throws DAOException {
		
		Piatto piatto = null;
		
		Connection conn = DBManager.getConnection();
		
		String sqlquery = "SELECT tipo,ingrediente,numeropezzi,prezzo FROM Piatti WHERE id=?";
		
		try ( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{
			
			stmt.setInt(1, id);

			try( ResultSet result = stmt.executeQuery(); )
			{
				while (result.next()) {
	        	
					TipoPiatto tipo = TipoPiatto.valueOf( result.getString(1) );
					Ingrediente ingrediente = Ingrediente.valueOf( result.getString(2) );
					int numeroPezzi = result.getInt(3);
					int prezzo = result.getInt(3);
					
					piatto = new Piatto(id, tipo, ingrediente, numeroPezzi, prezzo);
				}
			}
		}
		catch(SQLException e) {

			throw new DAOException("Errore SELECT Piatto");
		}
		
		return piatto;
	}
	
	
	public static ArrayList<Piatto> readAll() throws DAOException {
		
		ArrayList<Piatto> piatti_list = new ArrayList<Piatto>();
		
		Connection conn = DBManager.getConnection();
		
		String sqlquery = "SELECT id,tipo,ingrediente,numeropezzi,prezzo FROM Piatti";
		
		try ( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{

			try( ResultSet result = stmt.executeQuery(); )
			{
				while (result.next()) {
	        	
					int id = result.getInt(1);
					TipoPiatto tipo = TipoPiatto.valueOf( result.getString(2) );
					Ingrediente ingrediente = Ingrediente.valueOf( result.getString(3) );
					int numeroPezzi = result.getInt(4);
					int prezzo = result.getInt(5);
					
					Piatto piatto = new Piatto(id, tipo, ingrediente, numeroPezzi, prezzo);

					piatti_list.add(piatto);
				}
			}
		}
		catch(SQLException e) {

			throw new DAOException("Errore SELECT Piatto");
		}
		
		return piatti_list;
	}
	
	
	
	public static void update(Piatto piatto) throws DAOException {
		
		TipoPiatto tipo = piatto.getTipo();
		Ingrediente ingrediente = piatto.getIngrediente();
		int numeroPezzi = piatto.getNumeroPezzi();
		int prezzo = piatto.getPrezzo();
		int id_piatto = piatto.getId();
		
		Connection conn = DBManager.getConnection();
		
		String sqlquery = "UPDATE Piatti SET tipo=?, ingrediente=?, numeroPezzi=?, prezzo=? WHERE id=?;";

		try( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{
			stmt.setString(1, tipo.toString());
			stmt.setString(2, ingrediente.toString());
			stmt.setInt(3, numeroPezzi);
			stmt.setInt(4, prezzo);
			stmt.setInt(5, id_piatto);
			
			stmt.executeUpdate();
		}
		catch(SQLException e) {

			throw new DAOException("Errore UPDATE Piatto");
		}
	}
	
	public static void delete(Piatto piatto) throws DAOException {
		
		int id_piatto = piatto.getId();
		
		Connection conn = DBManager.getConnection();
		
		String sqlquery = "DELETE FROM Piatti WHERE id=?;";
		
		try( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{
			stmt.setInt(1, id_piatto);
			
			stmt.executeUpdate();
		}
		catch(SQLException e) {

			throw new DAOException("Errore DELETE Piatto");
		}
	}
}
