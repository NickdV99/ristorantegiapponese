package it.unina.ingsw.ristorantegiapponese.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;

import it.unina.ingsw.ristorantegiapponese.entity.Bevanda;
import it.unina.ingsw.ristorantegiapponese.entity.TipoBevanda;

public class BevandaDAO {

	public static Bevanda create(TipoBevanda tipo, int prezzo) throws DAOException {
		
		Bevanda bevanda = new Bevanda(tipo, prezzo);
		
		int id_bevanda = -1;
		
		Connection conn = DBManager.getConnection();

		String sqlquery = "INSERT INTO Bevande VALUES (NULL, ?, ?);";
		
		try( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{
			
			stmt.setString(1, tipo.toString());
			stmt.setInt(2, prezzo);
			
			stmt.executeUpdate();

			try( ResultSet result = stmt.getGeneratedKeys(); )
			{
		        if (result.next()) {
		        	id_bevanda = result.getInt(1);
		        }
			}
		}
		catch(SQLException e) {
			
			throw new DAOException("Errore INSERT Bevanda");
		}
		
		bevanda.setId(id_bevanda);

		return bevanda;
	}
	
	
	
	public static Bevanda read(Integer id) throws DAOException {
		
		Bevanda bevanda = null;
		
		Connection conn = DBManager.getConnection();
		
		String sqlquery = "SELECT tipo,prezzo FROM Bevande WHERE id=?";
		
		try ( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{
			
			stmt.setInt(1, id);

			try( ResultSet result = stmt.executeQuery(); )
			{
				while (result.next()) {
	        	
					TipoBevanda tipo = TipoBevanda.valueOf( result.getString(1) );
					int prezzo = result.getInt(2);
					
					bevanda = new Bevanda(id, tipo, prezzo);
				}
			}
		}
		catch(SQLException e) {

			throw new DAOException("Errore SELECT Bevanda");
		}
		
		return bevanda;
	}
	
	
	public static ArrayList<Bevanda> readAll() throws DAOException {
		
		ArrayList<Bevanda> bevande_list = new ArrayList<Bevanda>();
		
		Connection conn = DBManager.getConnection();
		
		String sqlquery = "SELECT id,tipo,prezzo FROM Bevande";
		
		try ( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{

			try( ResultSet result = stmt.executeQuery(); )
			{
				while (result.next()) {

					int id = result.getInt(1);
					TipoBevanda tipo = TipoBevanda.valueOf( result.getString(2) );
					int prezzo = result.getInt(3);
					
					Bevanda bevanda = new Bevanda(id, tipo, prezzo);
					
					bevande_list.add(bevanda);
				}
			}
		}
		catch(SQLException e) {

			throw new DAOException("Errore SELECT Bevanda");
		}
		
		return bevande_list;
	}
	
	
	public static void update(Bevanda bevanda) throws DAOException {
		
		TipoBevanda tipo = bevanda.getTipo();
		int prezzo = bevanda.getPrezzo();
		int id_bevanda = bevanda.getId();
		
		Connection conn = DBManager.getConnection();
		
		String sqlquery = "UPDATE Piatti SET tipo=?, prezzo=? WHERE id=?;";

		try( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{
			stmt.setString(1, tipo.toString());
			stmt.setInt(2, prezzo);
			stmt.setInt(3, id_bevanda);
			
			stmt.executeUpdate();
		}
		catch(SQLException e) {

			throw new DAOException("Errore UPDATE Bevanda");
		}
	}
	
	public static void delete(Bevanda bevanda) throws DAOException {
		
		int id_bevanda = bevanda.getId();
		
		Connection conn = DBManager.getConnection();
		
		String sqlquery = "DELETE FROM Bevanda WHERE id=?;";
		
		try( PreparedStatement stmt = conn.prepareStatement(sqlquery); )
		{
			stmt.setInt(1, id_bevanda);
			
			stmt.executeUpdate();
		}
		catch(SQLException e) {

			throw new DAOException("Errore DELETE Bevanda");
		}
	}
}
